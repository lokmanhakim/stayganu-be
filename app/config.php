<?php

use Illuminate\Database\Capsule\Manager as Capsule;

define('BASE_URL','http://localhost/stayganu/public');
define('PATH_UPLOADS', 'D:\Programs\XAMPP\htdocs\stayganu\public\uploads');
define('SALT_KEY', 'Ganukit3');

/**
 * Configure the database and boot Eloquent
 */
$capsule = new Capsule;

$capsule->addConnection(array(
    'driver'    => 'mysql',
    'host'      => 'localhost',
    'database'  => 'stayganu',
    'username'  => 'root',
    'password'  => '',
    'charset'   => 'utf8',
    'collation' => 'utf8_general_ci',
    'prefix'    => ''
));

$capsule->setAsGlobal();

$capsule->bootEloquent();

// set timezone for timestamps etc
date_default_timezone_set('UTC');
