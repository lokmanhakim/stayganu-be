<?php

use Illuminate\Database\Eloquent\Model as Eloquent;

class Facility extends Eloquent {

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'facilities';

	/**
	 * The attributes excluded from the model's JSON form.
	 *
	 * @var array
	 */

    public $timestamps = false;

	public function homestays()
	{
		return $this->hasMany('FacilityHomestay', 'facility_id', 'id');
	}

}
