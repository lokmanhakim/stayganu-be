<?php

use Illuminate\Database\Eloquent\Model as Eloquent;

class FacilityHomestay extends Eloquent {

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'facilities_homestays';

	/**
	 * The attributes excluded from the model's JSON form.
	 *
	 * @var array
	 */

    public $timestamps = false;

	public function facilities()
	{
		return $this->belongsTo('Facility', 'facility_id', 'id');

	}

	public function homestay()
	{
		return $this->belongsTo('Homestay', 'homestay_id', 'id');

	}

}
