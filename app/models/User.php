<?php

use Illuminate\Database\Eloquent\Model as Eloquent;

class User extends Eloquent {

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'users';

	/**
	 * The attributes excluded from the model's JSON form.
	 *
	 * @var array
	 */
	protected $hidden = array('password');

    public $timestamps = false;

	public function homestays()
	{
		return $this->hasMany('Homestay', 'created_by', 'id');
	}


    protected function setCreatedAtAttribute($value)
	{
		$this->attributes['created_at'] = strtotime($value);
	}

	protected function setLastActiveAttribute($value)
	{
		$this->attributes['last_active'] = strtotime($value);
	}

}
