<?php

use Illuminate\Database\Eloquent\Model as Eloquent;

class Homestay extends Eloquent {

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'homestays';

	/**
	 * The attributes excluded from the model's JSON form.
	 *
	 * @var array
	 */

    public $timestamps = false;

    public function users()
    {
        return $this->belongsTo('User', 'created_by', 'id');

    }

    public function facilities()
    {
        return $this->hasMany('FacilityHomestay', 'homestay_id', 'id');
    }

    public function images()
    {
        return $this->hasMany('Image', 'homestay_id', 'id');
    }

	protected function setCreatedAtAttribute($value)
	{
		$this->attributes['created_at'] = strtotime($value);
	}

}
