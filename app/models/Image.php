<?php
/**
 * Created by Lokman Azli.
 * lokmanhakim@live.com.my
 * Date: 22-Oct-15
 * Time: 12:13 PM
 */
use Illuminate\Database\Eloquent\Model as Eloquent;

class Image extends Eloquent {

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'images';

	/**
	 * The attributes excluded from the model's JSON form.
	 *
	 * @var array
	 */
	protected $hidden = array('');

	public $timestamps = false;

	public static function upload($ref)
	{
		$storage = new \Upload\Storage\FileSystem(PATH_UPLOADS);
		$file = new \Upload\File('img', $storage);

		// Optionally you can rename the file on upload
		$new_filename = uniqid();
		$file->setName($new_filename);

		// Validate file upload
		// MimeType List => http://www.iana.org/assignments/media-types/media-types.xhtml
		$file->addValidations(array(
			// Ensure file is of type "image/png"
			new \Upload\Validation\Mimetype('image/jpeg'),

			//You can also add multi mimetype validation
			//new \Upload\Validation\Mimetype(array('image/png', 'image/gif'))

			// Ensure file is no larger than 5M (use "B", "K", M", or "G")
			new \Upload\Validation\Size('8M')
		));


		// Try to upload file
		try {
			// Success!
			$file->upload();

			$img = new Image;
			$img->homestay_id = $ref;
			$img->filename = $file->getNameWithExtension();
            $img->save();
			return true;

		} catch (\Exception $e) {
			return  $e->getMessage();
		}
	}

	public function base64_to_jpeg($base64_string) {
		$decoded = base64_decode($base64_string);

		$new_filename = uniqid();
		file_put_contents('uploads/'.$new_filename.'.jpg',$decoded);
		return $new_filename.'.jpg';
	}

}