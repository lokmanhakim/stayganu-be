<?php

use Illuminate\Database\Eloquent\Model as Eloquent;

class Apikey extends Eloquent {

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'apikeys';

	/**
	 * The attributes excluded from the model's JSON form.
	 *
	 * @var array
	 */

    public $timestamps = false;

}
