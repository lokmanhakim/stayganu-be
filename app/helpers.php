<?php
/**
 * Created by Lokman Azli.
 * lokmanhakim@live.com.my
 * Date: 18-Feb-16
 * Time: 3:38 PM
 */

function api_response ($status, $message, $code) {
    $app = \Slim\Slim::getInstance();
    $app->response->setStatus($code);
    $app->contentType('application/json');
    $msg = array(
        'status' => $status,
        'message' => $message
    );
    echo json_encode($msg);
}