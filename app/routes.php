<?php
/**
 * Created by Lokman Azli.
 * lokmanhakim@live.com.my
 * Date: 18-Feb-16
 * Time: 6:26 PM
 */

use Carbon\Carbon;
/*************************
 * Route Filter
 *************************/

function AuthBasic() {
    $app = \Slim\Slim::getInstance();

    $user = User::where('username', $app->request->params('username'))->first();

    if (!$user || $user->token != $app->request->params('public_key')):
        $message = 'Authentication failure. Kindly verify your credential and try again';
        api_response('error', $message, 401);
        $app->stop();
    endif;
}

function AuthApi() {
    $app = \Slim\Slim::getInstance();

    $param = json_decode($app->request->getBody(),true);

    $authenticated = Apikey::where('app_secret', $param->app_secret)->first();

    if (!$authenticated):
        $message = 'There seems to be a problem verifying the API key. Please check & try again.';
        api_response('error', $message, 401);
        $app->stop();
    endif;
}

/*************************
 * Route here
 *************************/
$app->get('/', 'AuthApi', function () use ($app){
    $message = "Successfully connected";
    api_response('success', $message, 200);
});

/* API Group */
$app->group('/api', function () use ($app) {

    /**** Homestay Group ****/
    $app->group('/homestay', function () use ($app) {

        /* list all homestay */
        $app->get('/', function () use ($app) {

            $result = Homestay::with('images')->orderBy('created_at', 'desc')->get();
            $app->contentType('application/json');
            echo json_encode($result);

        });

        /* get single record */
        $app->get('/:id', function ($id) use ($app) {

            $result = Homestay::with('images')->find($id);
            $app->contentType('application/json');
            echo json_encode($result);

        });

        $app->put('/:id', function ($id) use ($app) {

            try {
                $hs = Homestay::find($id);
                $hs->name = $app->request->params('name');
                $hs->lat = $app->request->params('lat');
                $hs->lang = $app->request->params('lang');
                $hs->address = $app->request->params('address');
                $hs->descriptions = $app->request->params('descriptions');
                $hs->policy = $app->request->params('policy');
                $hs->accomodates = $app->request->params('accomodates');
                $hs->type = $app->request->params('type');
                $hs->updated_at = \Carbon\Carbon::now();
                $hs->updated_by = $app->request->params('user_id');

                if($hs->save()):

                    $message = 'Your homestay has been successfully updated';
                    api_response('success', $message, 200);
                endif;
            }
            catch(\Exception $e) {
                api_response('error', $e->getMessage(), 400);
            }

        });

        /* create new record */
        $app->post('/new', function () use ($app) {

            $param = json_decode($app->request->getBody(), true);

            try {
                $hs = new Homestay();
                $hs->name = $param['name'];
//                $hs->lat = $param['lat'];
//                $hs->lang = $param['lang'];
//                $hs->address = $param['address'];
                $hs->descriptions = $param['descriptions'];
//                $hs->policy = $param['policy'];
//                $hs->accomodates = $param['accomodates'];
                $hs->type = 1;
                $hs->created_at = \Carbon\Carbon::now();
                $hs->created_by = 1;

                if($hs->save()):

                    if(!is_null($param['image'])):
                        $img = new Image;
                        $img->homestay_id = $hs->id;
                        $img->filename = $img->base64_to_jpeg($param['image']);
                        $img->save();
                    endif;

                        $message = 'Your homestay has been successfully submitted to the listing';
                    api_response('success', $message, 200);
                endif;
            }
            catch(\Exception $e) {
                api_response('error', $e->getMessage(), 400);
            }

        });

        /* add image to homestay */
        $app->post('/image/:id', function($id) use ($app) {

            $param = json_decode($app->request->getBody(), true);

            try {
                $hs = Homestay::find($id);

                if( $hs && $param['image'] ):

                    $img = new Image;
                    $img->homestay_id = $id;
                    $img->filename = $img->base64_to_jpeg($param['image']);
                    $img->save();

                    $message = 'Homestay image has been successfully updated';
                    api_response('success', $message, 200);
                else:
                    api_response('error', 'Homestay not found or field img is not available', 400);
                endif;
            }
            catch(\Exception $e) {
                api_response('error', $e->getMessage(), 400);
            }

        });

    });

    /**** User Group ****/
    $app->group('/user', function () use ($app) {

        /* register new user */
        $app->post('/register', function() use($app) {

            /** @var  $user - check if record exist */
            $user = User::whereRaw('username ="'.$app->request->params('username').'" or email = "'.$app->request->params('email').'"')->first();

            if(empty($user)) {
                $user = new User;
                $user->username = $app->request->params('username');
                $user->email = $app->request->params('email');
                $user->password = password_hash($app->request->params('password'), PASSWORD_DEFAULT);
                $user->created_at = \Carbon\Carbon::now();

                if ($user->save()) {
                    $message = 'You has been successfully registered';
                    api_response('success', $message, 200);
                }
            }
            else {
                $message = 'Username or email is already taken';
                api_response('error', $message, 400);
            }

        });

        /* get single user */
        $app->get('/profile/:username', function ($username) use ($app) {

            $result = User::with('homestays', 'homestays.images')->where('username', $username)->first();
            $app->contentType('application/json');
            echo json_encode($result);

        });

    });

    /**** Auth Group ****/
    $app->group('/auth','AuthApi', function () use ($app) {

        /* login */
        $app->post('/login', function() use($app){

            $param = json_decode($app->request->getBody(),true);

            $username = $param['username'];
            $password = $param['password'];

            $user = \User::where('username', $username)->first();

            if(!empty($user))
            {
                if(password_verify($password, $user->password)) {

                    $user->token = md5(SALT_KEY.strtotime(Carbon::now()));
                    $user->last_active = Carbon::now();
                    $user->save();

                    $msg = array(
                        'data' => $user,
                        'status' => 'success',
                        'message' => 'You have successfully login');
                    $app->response->setStatus(200);
                    $app->contentType('application/json');
                    echo json_encode($msg);
                }
                else
                {
                    $message = 'Wrong credentials';
                    api_response('error', $message, 401);
                }
            }
            else {
                $message = $username.' You are not authorized.';
                api_response('error', $message, 401);
            }

        });

    });

});