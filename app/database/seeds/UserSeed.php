<?php

use Carbon\Carbon;


class UserSeed {

    function run()
    {
        $user = new User;
        $user->username = "superadmin";
        $user->email = "superadmin@email.com";
        $user->created_at = Carbon::now();
        $user->password = password_hash("abc123", PASSWORD_BCRYPT);
        $user->save();
    }
}
