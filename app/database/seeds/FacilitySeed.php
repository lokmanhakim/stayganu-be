<?php

class FacilitySeed {

    function run()
    {
        $facilities = array(
            'TV with Astro',
            'TV',
            'Washing Machine',
            'Water Heater',
            'Refrigerator',
            'Internet',
            'Pool',
            'Aircond',
            'Fan',
            'Kitchen'
        );

        foreach($facilities as $f):
            $fac = new Facility();
            $fac->description = $f;
            $fac->save();
        endforeach;
    }
}
