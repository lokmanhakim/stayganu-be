<?php

use Illuminate\Database\Capsule\Manager as Capsule;

/**
 * Example migration for use with "novice"
 */
class HomestayMigration {
    function run()
    {
        Capsule::schema()->dropIfExists('homestays');
        Capsule::schema()->create('homestays', function($table) {
            $table->increments('id');
            $table->string('name');
            $table->double('lat', 15, 8);
            $table->double('lang', 15, 8);
            $table->string('address');
            $table->text('descriptions');
            $table->string('policy');
            $table->tinyInteger('accomodates');
            $table->tinyInteger('type');
            $table->integer('created_at');
            $table->integer('created_by');
            $table->integer('updated_at');
            $table->integer('updated_by');
        });
    }
}
