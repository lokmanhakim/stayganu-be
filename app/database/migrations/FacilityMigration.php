<?php

use Illuminate\Database\Capsule\Manager as Capsule;

/**
 * Example migration for use with "novice"
 */
class FacilityMigration {
    function run()
    {
        Capsule::schema()->dropIfExists('facilities');
        Capsule::schema()->create('facilities', function($table) {
            $table->increments('id');
            $table->string('description');
        });
    }
}
