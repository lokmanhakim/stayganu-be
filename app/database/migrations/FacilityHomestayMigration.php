<?php

use Illuminate\Database\Capsule\Manager as Capsule;

/**
 * Example migration for use with "novice"
 */
class FacilityHomestayMigration {
    function run()
    {
        Capsule::schema()->dropIfExists('facilities_homestays');
        Capsule::schema()->create('facilities_homestays', function($table) {
            $table->increments('id');
            $table->integer('facility_id');
            $table->integer('homestay_id');
            $table->integer('created_at');
            $table->integer('created_by');
//            $table->foreign('facility_id')->references('id')->on('facilities');
//            $table->foreign('homestay_id')->references('id')->on('homestay');
        });
    }
}
