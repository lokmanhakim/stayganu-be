<?php

use Illuminate\Database\Capsule\Manager as Capsule;

/**
 * Example migration for use with "novice"
 */
class ApikeyMigration {
    function run()
    {
        Capsule::schema()->dropIfExists('apikeys');
        Capsule::schema()->create('apikeys', function($table) {
            $table->increments('id');
            $table->string('app_name');
            $table->string('app_secret');
        });
    }
}
