<?php

use Illuminate\Database\Capsule\Manager as Capsule;

/**
 * Example migration for use with "novice"
 */
class UserMigration {
    function run()
    {
        Capsule::schema()->dropIfExists('users');
        Capsule::schema()->create('users', function($table) {
            $table->increments('id');
            $table->string('username', 20)->unique();
            $table->string('email', 100)->unique();
            $table->string('password');
            $table->string('name');
            $table->string('contact');
            $table->string('token');
            $table->integer('created_at');
            $table->integer('last_active');
        });
    }
}
