<?php

require __DIR__.'/../vendor/autoload.php';

$app = new Slim\Slim(array(
    'debug' => true
));

require '../app/routes.php';

/** @var  $corsOptions  setup CORS */
$corsOptions = array(
    "origin" => array("http://localhost", "http://localhost:8080", "http://localhost:8100"),
    "allowMethods" => "GET,HEAD,PUT,POST,DELETE",
    "maxAge" => 1728000,
    "allowHeaders" => array("X-Requested-With", "accept", "content-type")
);
$cors = new \CorsSlim\CorsSlim($corsOptions);
$app->add($cors);


$app->run();
